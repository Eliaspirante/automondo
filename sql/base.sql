CREATE TABLE IF NOT EXISTS `automondo`.`veiculos` (
  `id`                      INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_modelo`               INT(10) UNSIGNED NOT NULL,
  `id_marca`                INT(10) UNSIGNED NOT NULL,
  `nome`                    VARCHAR(255)     NOT NULL,
  `qtd_portas`              INT(2) DEFAULT NULL,
  `placa`                   VARCHAR(10) DEFAULT NULL,
  `placa_fim`               VARCHAR(2) DEFAULT NULL,
  `versao`                  VARCHAR(255) DEFAULT NULL,
  `ano_modelo`              VARCHAR(4) DEFAULT NULL,
  `ano_fabricao`            VARCHAR(4) DEFAULT NULL,
  `observacao`              TEXT DEFAULT NULL,
  `estado_veiculo`          INT(1) DEFAULT 0,
  `blindado`                INT(1) DEFAULT 0,
  `combustivel`             VARCHAR(255) DEFAULT NULL,
  `transmissao`             VARCHAR(255) DEFAULT NULL,
  `preco`                   FLOAT(11)        DEFAULT 0.0,
  `kilometragem`            VARCHAR(255)      DEFAULT 0,
  `chassis`                 VARCHAR(255)      DEFAULT NULL,
  `chassis2`                VARCHAR(255)      DEFAULT NULL,
  `ipva_pago`               VARCHAR(1)      DEFAULT NULL,
  `revisado_concessionaria` VARCHAR(1)      DEFAULT NULL,
  `garantia_fabrica`        VARCHAR(1)      DEFAULT NULL,
  `licenciado`              VARCHAR(1)      DEFAULT NULL,
  `batido`                  VARCHAR(1)      DEFAULT NULL,
  `entrada_facilitada`      VARCHAR(1)      DEFAULT NULL,
  `air_bag`                 VARCHAR(1)      DEFAULT NULL,
  `air_bag_motorista`       VARCHAR(1)      DEFAULT NULL,
  `air_bag_duplo`           VARCHAR(1)      DEFAULT NULL,
  `ar_condicionado`         VARCHAR(1)      DEFAULT NULL,
  `ar_quente`               VARCHAR(1)      DEFAULT NULL,
  `desembacador_traseiro`   VARCHAR(1)      DEFAULT NULL,
  `cd_player`               VARCHAR(1)      DEFAULT NULL,
  `dvd_player`              VARCHAR(1)      DEFAULT NULL,
  `radio`                   VARCHAR(1)      DEFAULT NULL,
  `toca_fitas`              VARCHAR(1)      DEFAULT NULL,
  `freios_abs`              VARCHAR(1)      DEFAULT NULL,
  `rodas_liga_leve`         VARCHAR(1)      DEFAULT NULL,
  `trava_eletrica`          VARCHAR(1)      DEFAULT NULL,
  `vidros_eletricos`        VARCHAR(1)      DEFAULT NULL,
  `alarme`                  VARCHAR(1)      DEFAULT NULL,
  `banco_couro`             VARCHAR(1)      DEFAULT NULL,
  `direcao_hidraulica`      VARCHAR(1)      DEFAULT NULL,
  `limpador_traseiro`       VARCHAR(1)      DEFAULT NULL,
  `teto_solar`              VARCHAR(1)      DEFAULT NULL,
  `opcionais`               TEXT    DEFAULT NULL,
  `active`                  INT(1)           NOT NULL DEFAULT 1,
  `created_at`              DATETIME         NOT NULL,
  `updated_at`              DATETIME         NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `automondo`.`imagens_veiculos` (
  `id`           INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao`         VARCHAR(255)     NOT NULL,
  `caminho`         VARCHAR(255)     NOT NULL,
  `active`       INT(1)           NOT NULL DEFAULT 1,
  `created_at`   DATETIME         NOT NULL,
  `updated_at`   DATETIME         NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `automondo`.`marca_veiculos` (
  `id`           INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome`         VARCHAR(255)     NOT NULL,
  `active`       INT(1)           NOT NULL DEFAULT 1,
  `created_at`   DATETIME         NOT NULL,
  `updated_at`   DATETIME         NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `automondo`.`modelo_veiculos` (
  `id`           INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_marca`     INT(10) UNSIGNED NOT NULL,
  `nome`         VARCHAR(255)     NOT NULL,
  `active`       INT(1)           NOT NULL DEFAULT 1,
  `created_at`   DATETIME         NOT NULL,
  `updated_at`   DATETIME         NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;