<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	function __construct(){
        parent::__construct();

        $this->load->model("Veiculos_m");
        $this->load->model("MarcaVeiculos_m");
        $this->load->model("ModeloVeiculos_m");
        $this->load->model("ImagensVeiculos_m");
    }

	public function index()
	{
		$this->load->view('index_page');
	}

	public function veiculos($marca = null, $modelo = null){

	}

	public function veiculo($id_veiculo = null){

	}

	public function updateVeiculos(){
		$xmldata = simplexml_load_file( 'https://sistemaautoweb.com.br/download/anuncios/AutoWeb/257.xml' );

    	if(!isset($xmldata) || !isset($xmldata->Veiculos) || !isset($xmldata->Veiculos->Veiculo) || count($xmldata->Veiculos->Veiculo) == 0){
			die( '<p style="color:red">Atenção: Nenhum registro encontrado para inserção!</p>' );
			return;
		}

		$marcas = $this->MarcaVeiculos_m->get_all();
		$modelos = $this->ModeloVeiculos_m->get_all();

		if(!$marcas){
			$marcas = array();
		}
		if(!$modelos){
			$modelos = array();
		}

		foreach( $xmldata->Veiculos->Veiculo as $veiculo ){
			$idMarca = null;
			$idModelo = null;

			try{
				if(!in_array($veiculo->Marca, $marcas)){
					$idMarca = $this->MarcaVeiculos_m->insert(array('nome' => $veiculo->Marca));
				}else{
					$idMarca = $marcas[];
				}

				if(!in_array($veiculo->Modelo, $modelos)){
					$idModelo = $this->ModeloVeiculos_m->insert(array('nome' => $veiculo->Modelo, 'id_marca' => $idMarca));
				}else{
					$idModelo = $modelos[];
				}
			
				if(!isset($idMarca) || !isset($idModelo)){
					echo "Erro durante inserção de veículo: ";
					print_r($veiculo);
					continue;
				}

				$opArray = array(
					'IPVA Pago' => ( string ) $veiculo->IPVAPago,
					'Revisado Concessionaria' => ( string ) $veiculo->RevisadoConcessionaria,
					'Garantia Fabrica' => ( string ) $veiculo->GarantiaFabrica,
					'Licenciado' => ( string ) $veiculo->Licenciado,
					'Batido' => '',
					'Entrada Facilitada' => '',
					'AirBag' => ( string ) $veiculo->AirBag,
					'AirBag Motorista' => ( string ) $veiculo->AirBagMotorista,
					'AirBag Duplo' => ( string ) $veiculo->AirBagDuplo,
					'Ar Condicionado' => ( string ) $veiculo->ArCondicionado,
					'Ar Quente' => ( string ) $veiculo->ArQuente,
					'Desembacador Traseiro' => ( string ) $veiculo->DesembacadorTraseiro,
					'CD Player' => ( string ) $veiculo->CDPlayer,
					'DVD Player' => ( string ) $veiculo->DVDPlayer,
					'Radio' => ( string ) $veiculo->Radio,
					'Toca Fitas' => ( string ) $veiculo->TocaFitas,
					'Freios ABS' => ( string ) $veiculo->FreiosABS,
					'Rodas Liga Leve' => ( string ) $veiculo->RodasLigaLeve,
					'Trava Eletrica' => ( string ) $veiculo->TravaEletrica,
					'Vidros Eletricos' => ( string ) $veiculo->VidrosEletricos,
					'Alarme' => ( string ) $veiculo->Alarme,
					'Banco Couro' => ( string ) $veiculo->BancoCouro,
					'Direcao Hidraulica' => ( string ) $veiculo->DirecaoHidraulica,
					'Limpador Traseiro' => ( string ) $veiculo->LimpadorTraseiro,
					'Teto Solar' => ( string ) $veiculo->TetoSolar
				);
				
				$opFinal = "";
						
				foreach ($opArray as $key => $value){
					
					if($value == "S"){
						$opFinal .= $key;
						
						if ($key != end(array_keys($opArray))){
						   $opFinal .= ', ';
						}
						
					}
					
				}
				$array_data = array(
					'id_marca' => $idMarca,
					'id_modelo' => $idModelo,
					'nome' => $veiculo->Marca . ' ' . $veiculo->Modelo,
					'qtd_portas' => $veiculo->QtdPortas,
					'placa' => ( string )$veiculo->Placa,
					'placa_fim' => substr( trim( ( string )$veiculo->Placa ), -1),
					'versao' => ( string ) $veiculo->Versao,
					'ano_modelo' => ( string ) $veiculo->AnoModelo,
					'ano_fabricao' => ( string ) $veiculo->AnoFabricacao,
					'observacao' => ( string ) $veiculo->Observacao,
					'estado_veiculo' => ( string ) $veiculo->UsadoNovo,
					'blindado' => ( string ) $veiculo->Blindado;,
					'combustivel' => ((( string ) $veiculo->Combustivel) == 'GASOLINA E ÁLCOOL'?(string)$veiculo->Combustivel:'Flex'),
					'transmissao' => ( string ) $veiculo->Transmissao,
					'preco' => $veiculo->Preco,
					'kilometragem' => ( string ) $veiculo->Kilometragem,
					'chassis' => $veiculo->Chassis,
					'chassis2' => $veiculo->Chassis2,
					'opcionais' => $opFinal
				);

				$veiculos_consulta = $this->Veiculos_m->where('chassi',$veiculo->Chassi)->get_all();

				if(count($veiculos_consulta) > 0){
					$this->Veiculos_m->update($veiculos_consulta[0]->id, $array_data);
				}else{
					$this->Veiculos_m->insert($array_data);
				}

			}catch(Exception $e){
				echo "ERRO: ".$e->getMessage()."\n";
			}
		}
	}
}
