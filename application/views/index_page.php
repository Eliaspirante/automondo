<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Automondo – Com sede própria em Campinas a Automondo Veículos é referência no mercado de veículos semi-novos com qualidade e procedência comprovada. Venha conhecer.</title>

	</head>
	<body>
		<div id="menu_superior">
			<ul>
				<li><a href="#localizacao">LOCALIZAÇÃO</a></li>
				<li><a href="#">EMPRESA</a></li>
				<li><a href="#">ESTOQUE</a></li>
				<li><a href="#">CONTATO</a></li>
			</ul>

			<ul class="menu-social">
                <li class="facebook"><a href="https://www.facebook.com/Automondo-Ve%C3%ADculos-297828463569971/" target="_blank">Facebook</a></li>
                <li class="instagram"><a href="https://www.instagram.com/automondo/" target="_blank">Instagram</a></li>
                <li class="telefone"><a href="tel:1937227000" target="_blank">Telefone</a></li>
            </ul>
		</div>

		<div id="div_busca">
			<p>ENCONTRE O SEU VEÍCULO:</p>
			<form id="form_busca">
				<select id="marca">
					<option>Marca</option>
				</select>
				<select id="modelo">
					<option>Modelo</option>
				</select>

				<label for="blindado">Blindado</label>
				<input id="blindado" type="checkbox"/>
				
				<p>Preço</p>
				<input id="preco_de" placeholder="De" />
				<input id="preco_ate" placeholder="Até" />

				<p>Ano</p>
				<select id="ano_de">
					<option>De</option>
				</select>
				<select id="ano_ate">
					<option>Até</option>
				</select>

				<p>
					<button>BUSCAR</button>
				</p>
			</form>
		</div>

		<div id="resultados">
			<?php if(isset($carros)): ?>
				<?php foreach($carros as $carro): ?>
					<div id="card">
						<?php echo $carro->titulo; ?>
						<?php echo $carro->imagem_destaque; ?>
						<?php echo $carro->descricao; ?>
						<?php echo $carro->ano; ?>
						<?php echo $carro->valor; ?>
					</div>
				<?php endforeach; ?>
			<?php endif;?>
			<button>Ver Mais</button>
		</div>

		<div id="showroom">
			SHOWROOM
		</div>

		<div id="contato">
			<form id="form_contato">
				<input type="text" id="nome"/>
				<input type="text" id="email"/>
				<input type="text" id="assunto"/>
				<input type="text" id="mensagem"/>

				<button>Enviar</button>
			</form>
			

			<p id="localizacao">
				<iframe height="450" frameborder="0" width="100%" allowfullscreen="" style="border: 0px none; pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3866.217034863241!2d-47.059426910070485!3d-22.888763090802744!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xded1011e78a8d043!2sComercial+Automondo+de+Ve%C3%ADculos+Ltda!5e0!3m2!1spt-BR!2sbr!4v1464888127570"></iframe>
			</p>

			<p>Endereço e telefone</p>
		</div>

		<div id="footer">
			<ul class="menu-social">
                <li class="facebook"><a href="https://www.facebook.com/Automondo-Ve%C3%ADculos-297828463569971/" target="_blank">Facebook</a></li>
                <li class="instagram"><a href="https://www.instagram.com/automondo/" target="_blank">Instagram</a></li>
                <li class="telefone"><a href="tel:1937227000" target="_blank">Telefone</a></li>
            </ul>
            <p class="copyright">Automondo © 2016 - Todos os direitos reservados.</p>
            <p class="assinatura"><a target="_blank" href="http://www.lema.ag"><strong>Agência Digital</strong> Lema</a></p>
		</div>
	</body>
</html>