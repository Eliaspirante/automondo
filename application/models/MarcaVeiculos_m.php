<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MarcaVeiculos_m extends MY_Model{
    public $table = 'marca_veiculos';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

}