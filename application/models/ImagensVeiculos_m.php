<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ImagensVeiculos_m extends MY_Model{
    public $table = 'imagens_veiculos';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

}