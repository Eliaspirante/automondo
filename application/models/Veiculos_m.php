<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculos_m extends MY_Model{
    public $table = 'veiculos';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

}